# Установка Ubuntu/Windows #

## Автозагрузка windows (OSLoader) ##

* `bootrec /fixmbr`

## Автозагрузка ubuntu (grub) ##

* `sudo mount /dev/sdXY /mnt`
* `sudo grub-install --boot-directory=/mnt/boot /dev/sdX`

## Создание образа Ubuntu для VirtualBox (Windows) ##

* `wmic diskdrive list brief /format:list`
* `cd "C:\Program Files\Oracle\VirtualBox"`
* `VBoxManage internalcommands createrawvmdk -filename "C:\Users\\***\Desktop\Ubuntu.vmdk" -rawdisk \\\\.\PhysicalDrive0`

*Статья: http://lifehacker.com/how-to-dual-boot-and-virtualize-the-same-partition-on-y-493223329*

## Установка самых необходимых пакетов ##

+ сначала это:

    * `sudo apt-get update`
    * `sudo apt-get upgrade`
    * `sudo apt-get -y purge modemmanager`
    * `sudo apt-get -y install screen ipython3 nmap python3-pip x11vnc pv git openssh-server`
    * `sudo -H pip3 install --upgrade pip`

+ там, где используются соответствующие питонячие модули:

    * `sudo -H pip3 install pyBarcode`
    * `sudo -H pip3 install evdev`
    * `sudo -H pip3 install modbus_tk`
    * `sudo -H pip3 install qrcode`
    * `sudo -H apt-get -y install sqlite3` (при использовании SQLite)
    * `sudo -H pip3 install psycopg2`

+ установка вима с необходимыми плагинами:

    * sudo apt-get -y install vim
    * git clone https://github.com/tswr/.vimrc.git ~/vim-tswr/
    * git clone https://github.com/gmarik/Vundle.vim.git ~/vim-tswr/.vim/bundle/Vundle.vim
    * ln -Fs ~/vim-tswr/.vim ~
    * ln -fs ~/vim-tswr/.vimrc ~
    * vim +PluginInstall +qall
    * cd
    * git clone https://github.com/nvol/vimchik.git
    * cp vimchik/.vimrc ~/

## Выдаем права пользователю (lockerbox) к serial портам (Ubuntu) ##

* `sudo adduser lockerbox dialout`

## Сносим злосчастный модем-менеджер ##

* `sudo apt-get purge modemmanager`

## Пробрасываем порт для RFID Z2 ##

* `sudo modprobe ftdi_sio`
* `sudo bash -c "echo 0403 1234 > /sys/bus/usb-serial/drivers/ftdi_sio/new_id"`

## RFID-сканер Smartec ##

* `sudo vim /etc/udev/rules.d/90-smartec-rfid.ru`
* Содержимое файла `SUBSYSTEM=="input", ENV{ID_VENDOR}=="Sycreader", MODE="0666"`

## Создаем symlink для Казначея (не используется) ##

* `sudo vim /etc/udev/rules.d/90-payment-devices.rules`
* Содержимое файла:
```
SUBSYSTEM=="tty", ATTRS{bInterfaceNumber}!="00", GOTO="atol_end"
SUBSYSTEM=="tty", ATTRS{idVendor}=="2912", ATTRS{idProduct}=="0005", SYMLINK+="fiscalPrinter"
LABEL="atol_end"
```

## Калибровка Touch Screen ##

* `sudo apt-get install xinput-calibrator`
* `xinput_calibrator` (при наличии подключенного сенсорного стекла)

## Выключаем уведомления в Ubuntu ##

* `sudo mv /usr/share/dbus-1/services/org.freedesktop.Notifications.service /usr/share/dbus-1/services/org.freedesktop.Notifications.service.disabled`

## LED подсветка в ключницах ##

* sudo -H pip3 install adafruit-circuitpython-neopixel
* Внимание! Запускать под правами супер-пользователя (Необходимо для RasPi peripherals)

# RaspPi #

## DHCP сервер ##

* Статья: https://www.raspberrypi.org/learning/networking-lessons/lesson-3/plan/

# Установка Python + Qt + SIP + PyQt #

## 1. Устанавливаем Python 3 ##

* `sudo apt-get install python3-dev`

## 2. Устанавливаем Qt 5.8 ##

* `wget http://download.qt.io/archive/qt/5.8/5.8.0/qt-opensource-linux-x64-5.8.0.run`
* `chmod u+x qt-opensource-linux-x64-5.8.0.run`
* `./qt-opensource-linux-x64-5.8.0.run`

## 3. Устанавливаем доп. модули ##

* `sudo apt-get install -y build-essential libgl1-mesa-dev libx11-dev libxext-dev libxfixes-dev libxi-dev libxrender-dev libxcb1-dev libx11-xcb-dev libxcb-glx0-dev libfontconfig1-dev libfreetype6-dev libglu1-mesa-dev libssl-dev libcups2-dev libpulse-dev`

## 4. Устанавливаем SIP ##

* `wget http://sourceforge.net/projects/pyqt/files/sip/sip-4.19.2/sip-4.19.2.tar.gz`
* `tar -xvzf sip-4.19.2.tar.gz`
* `cd sip-4.19.2`
* `python3 configure.py`
* `sudo make`
* `sudo make install`
* `sudo make clean`

## 5. Устанавливаем PyQt 5.8 ##

* `wget https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-5.8/PyQt5_gpl-5.8.tar.gz`
* `tar -xvzf PyQt5_gpl-5.8.tar.gz`
* `cd PyQt5_gpl-5.8`
* `python3 configure.py --destdir /usr/local/lib/python3.5/dist-packages/ --qmake ~/Qt5.8.0/5.8/gcc_64/bin/qmake --enable QtCore --enable QtGui --enable QtMultimedia --enable QtQml --enable QtNetwork --no-tools --confirm-license`
* `sudo make -j 5`
* `sudo make install`
* `sudo make clean`

# Browser LockerBox #

## Установка ##

* sudo apt-get install gir1.2-webkit-3.0
* sudo apt-get install gir1.2-gtk-3.0
* sudo apt-get install python3-gi

# Web-сервер (Apache + PHP) #

## Установка Apache ##

* `sudo apt-get install apache2`
* `sudo a2enmod rewrite`
+ `sudo nano /etc/apache2/sites-enabled/000-default.conf`
    * Вставляем код ниже внутрь блока _<VirtualHost *:80>_
```
#!Apache
<Directory /var/www/html>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
</Directory>
```
* `sudo nano /etc/apache2/envvars`
+ ***Запуск Apache2 под пользователем pi***
```
export APACHE_RUN_USER=pi
export APACHE_RUN_GROUP=pi
```
+ ***Логи Apache2 храним на usb-флешке***
```
export APACHE_LOG_DIR=/mnt/lbx/apache2_logs$SUFFIX
```

## Установка PHP ##

* `sudo apt-get install php7.0 libapache2-mod-php7.0 php7.0-curl php7.0-pgsql php7.0-sqlite`
+ `sudo nano /etc/apache2/mods-enabled/dir.conf` (Меняем приоритет файлов)
    * DirectoryIndex index.html index.cgi index.pl **index.php** index.xhtml index.htm
    * DirectoryIndex **index.php** index.html index.cgi index.pl index.xhtml index.htm

## Перезагружаем Apache ##

* `sudo service apache2 restart`

## Убираем Apache из автозагрузки ##

* sudo update-rc.d apache2 remove
* и помещаем в загрузку после маунта SD

### RPi ###

* ~/edit_autostart
```
@sudo -S service apache2 start
@sleep 2
@/home/pi/lausc
```

### OrangePI ###

* crontab -e
```
@reboot sleep 10; sudo -S service apache2 start
```

# Установка/Настройка PostgreSQL #

## Установка python3 расширение ##

### x64 ###

* `pip3 install psycopg2`

### RaspPi ###

* `sudo apt-get install libpq-dev`
* `sudo apt-get install python3-psycopg2`

## Установка psql server ##

* `sudo apt-get install postgresql postgresql-contrib`

## Создаем пользователя и БД (Пример: Логин - lockerbox, Пароль - goaway, База данных - lockers) ##

* `sudo -u postgres createuser lockerbox`
* `sudo -u postgres createdb lockers`
* `psql postgres` и выполяем там: _ALTER USER lockerbox PASSWORD 'goaway';_ (После выходим, выполнив `\q`)

## Внешний доступ ##

* `cd /etc/postgresql/9.5/main`
* `sudo nano postgresql.conf`
* Найти _#listen_addresses = 'localhost'_ и заменить на _listen_addresses = '*'_ (Закрыть и сохранить)
* `sudo nano pg_hba.conf`
* Найти строчка _local all all peer_ и заменить _peer_ на _md5_
* Найти ниже _host all all 127.0.0.1/32_ peer и добавить ниже _host all all 0.0.0.0/0 md5_ (Закрыть и сохранить)

## Перезагружаем PostgreSQL ##

* `sudo service postgresql restart`

# Другое #

## Waveshare 7' монитор ##

* `wget http://www.waveshare.com/w/upload/0/00/LCD-show-170703.tar.gz`
* `tar -xvzf LCD-show-170703.tar.gz`
* `chmod +x LCD7-1024x600-show`
* `./LCD7-1024x600-show`

*Статья: http://www.waveshare.com/wiki/7inch_HDMI_LCD*

## Master Touch (USB) ##

* `wget http://tronnes.org/downloads/xinput-calibrator_0.7.5-1_armhf.deb`
* `sudo dpkg -i -B xinput-calibrator_0.7.5-1_armhf.deb`
* `rm xinput-calibrator_0.7.5-1_armhf.deb`

## ntfs drive + raspberry-pi ##

* `sudo apt-get install ntfs-3g`
* `mount -t ntfs-3g -o umask=000 /dev/sda1 /mnt/lbx`

## Если не запускается gparted ##

* `xhost +local:`
* `gparted-pkexec`

## Выключаем cursor на RaspPi ##

* `sudo nano /etc/lightdm/lightdm.conf`
* расскоментировать строчку _xserver-command=X -nocursor_

## Прошивка принтера под Казначей ##

Обновление прошивки принтера SNBC BT-T080R

Чтобы узнать текущую версию прошивки: выключить принтер → нажать и удерживать кнопку FEED → подключить к принтеру питание → после начала печати отпустить кнопку FEED → принтер напечатает страницу ***BT-T080R TEST FORM***. В верхней части чека будет информация о версии прошивки, например Main Firmware: FV1.040 где FV1.040 - версия прошивки.
Если вместо данной страницу напечатано MAIN MENU, необходимо два раза кратковременно нажать кнопку FEED (соответствует меню Print Self Test), после чего еще раз нажать и удерживать кнопку FEED не менее 1 с, для подтверждения выбранного действия. Принтер напечатает страницу ***BT-T080R TEST FORM***.

Установите драйвер принтера Setup_BT-T080R V1.10.
Перед началом установки драйвера отключить интерфейсный USB-кабель принтера от компьютера, на котором будет производится установка драйвера. В процессе инсталляции по запросу установщика драйвера подключить USB-кабель принтера к компьютеру.

После окончания установки драйвера принтера зайти в список доступных принтеров, убедиться, что принтер BT-T080R(U)1 присутствует в списке, выполнить печать тестовой страницы: правой кнопкой мыши нажать на иконку принтера → Свойства принтера → вкладка Общие → Пробная печать. Если тестовая страница успешно была распечатана, переходите к следующему пункту.

Скачайте программу обновления принтера KIOSKUtility V2.67. 
Запустите файл KIOSKUtility.exe в папке KIOSKUtility V2.67. Далее нажимаем File → Open port → вверху выбираем модель принтера BT-T080R → выбрать USB → Ok.
Выбираем Printer Status → Printer Infor → Ок. Появится информация о ККТ, где Firmware Printer: FVx.xxx - версия прошивки принтера.
Выбираем Setting → Download → Browse → выбрать файл BT-T080R_FV1040_KSKQV12_MAIN_T06 → Download.
После завершения процедуры появится окно с сообщением Download  Success! Please reopen the port. Нажать ОК → Exit → Exit.
Далее необходимо загрузить файл конфигурации. Выбираем Setting → Download → Browse → меняем тип файла на All Files → выбрать файл Configuration setting by feed key button → Download.
После завершения процедуры появится окно с сообщением Download  Success! Нажать ОК → Exit → Exit.

Убедиться, что статусный светодиод принтера постоянно горит зеленым светом, отключить кабель питания.
Подключить интерфейсный USB-кабель и кабель питания, после чего убедиться, что версия прошивки изменилась.

После перепрошивки необходимо выставить настройки принтера в соответствии с теми, что представлены на фото (фото см. по ссылке):
http://ibb.co/bXQ5J5

Подробнее: https://казначей.онлайн/forum/index.php?topic=9.msg99#msg99

## Настройка NV200/Smart Payout ##

Для подключения к программе ITL Validator Manager необходимо, чтобы купюрник работал по протоколу ESSP.
Если он настроен на другой протокол, то его можно сбросить в ESSP путём перевода дип-свитча №8 из положения OFF в положение ON и обратно. Делать это надо при работающем устройстве (питание должно быть подано) и на том дип-свитче, который находится на приставке, принимающей купюры (у пэйаута свои дип-свитчи).
После того, как программа обнаружит устройство, она получит от него параметры и выведет их на экран. Для корректной работы с нашими софтами по CCTalk требуется выбрать протокол CCTalk, установить адрес устройства равный 2 и выбрать 8-битный проверочный код у пакетов.
Перевод в протокол CCTalk осуществляется нажатием кнопки применения изменений.

Проверка работы устройства осуществляется отправкой на него следующих пакетов:

* широковещательный пакет запроса кода изделия: [0, 0, 1, 244, 11]
* тот же пакет по адресу 2: [2, 0, 1, 244, 9]
* разрешить все каналы: [2, 2, 1, 231, 255, 255, 22]
* начать приём купюр: [2, 1, 1, 228, 1, 23]
* принять купюру: [2, 1, 1, 154, 1, 97]
* отвергнуть купюру: [2, 1, 1, 154, 0, 98]
* прочитать статус: [2, 0, 1, 29, 224]

Если последняя команда (прочитать статус) не сработала (не пришёл ответ), то следует насторожиться...
Возможно, что купюрник не работает с пэйаутом (не та прошивка?).


**Прошивка: https://drive.google.com/open?id=0B-ntnXhF_icbR2lfRS1HdmktUkE

## Установка VNC ##

* `sudo apt-get install x11vnc`
* убеждаемся, что переменная окружения DISPLAY задана верно
* `x11vnc -usepw`
* задаём пароль (наш стандартный, два раза)
* готовечко!

## Настройка домашних скриптов и автозапуск ##

### Настроим автозапуск: ###

##### Ubuntu (OLD) #####

* Поиск (search your computer) - кнопочка в левом верхнем углу на рабочем столе Ubuntu.
* Набираем startup или автозапуск (если Ubuntu русская) и запускаем гуй Startup Applications.
* Заводим новый элемент автозапуска, указываем lausc в домашней папке.

##### Ubuntu (NEW) #####

* Создать файл командой `sudo nano /lib/systemd/system/lbx-core.service` с содержимым:
```
[Unit]
Description=LockerBox Core
After=multi-user.target

[Service]
Type=simple
User=lockerbox
Environment=DISPLAY=:0
ExecStart=/usr/local/bin/lbx-core
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
```
* `sudo chmod 644 /lib/systemd/system/lbx-core.service`
* Создать файл командой `sudo nano /lib/systemd/system/x11vnc.service` с содержимым:
```
[Unit]
Description=Start x11vnc at startup.
After=multi-user.target

[Service]
Type=simple
User=lockerbox
Environment=DISPLAY=:0
# WorkingDirectory=/home/pi/
ExecStart=/usr/bin/x11vnc -usepw -noxdamage -norepeat
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
```
* `sudo chmod 644 /lib/systemd/system/x11vnc.service`
* `sudo systemctl daemon-reload`
* `sudo systemctl enable lbx-core.service`
* `sudo systemctl start lbx-core.service`
* `sudo systemctl enable x11vnc.service`
* `sudo systemctl start x11vnc.service`
* Создать файл командой `sudo nano /usr/local/bin/lbx-core` с содержимым:
```
#!/bin/bash
#
# LockerBox Ltd.
#

# Disable any form of screen saver / screen blanking / power management
# XSET=/usr/bin/xset
# $XSET s off
# $XSET s noblank
# $XSET -dpms

# NTPD service?
# Run here

# run proj
~/proj/run.py
```
* `sudo chmod +x /usr/local/bin/lbx-core`

##### RaspPi #####

* `~/edit_autostart` если есть, либо найти в homescripts других проектов

##### Armbian #####

* `crontab -e`
* Добавлем задачу: `@reboot /home/pi/lausc`

### Настроим домашние скрипты: ###

* В домашней папке должны быть все необходимые скрипты запуска скринов (типа core, web, k_web, dog, vnc) и скрипты входа в эти скрины (типа 1, 2, 3, 5,...).
* Если их нет, то нужно взять из какого-нибудь (лучше родного) проекта из папки homescripts и при необходимости отредактировать.
* Убедиться, что в скриптах core, web, dog прописаны правильные пути к текущему проекту.
* Зайти в scripts_to_launch.py и проверить список запускаемых скриптов, при необходимости отредактировать его (закомментировать лишнее; к примеру, не во всех проектах используется k_web, web и даже vnc).
* Ещё можно залезть в dog.py в папке проекта и проверить, что в нём правильно прописано название проекта PROJ_NAME.
* эээ... вроде всё

## Определение платы CYCKIT (красненькая такая) как устройство ACM ##

* Для начала следует ещё раз убедиться, что modemmanager снесён напрочь: `sudo apt-get purge modemmanager`

##### Ubuntu #####

* Занести драйвер cytherm в блэклист: `sudo echo "blacklist cytherm" > /etc/modprobe.d/blacklist.conf`

##### RaspPi #####

* Занести драйвер cytherm в блэклист: `sudo echo "blacklist cytherm" > /etc/modprobe.d/cytherm.conf`

## Форматирование флешки ##

* отключаем все сервисы (apache2, dnsmasq, etc.) - `sudo service [сервис] stop`
* `sudo umount /mnt/lbx`
* `sudo umount /dev/sda1`
* `sudo mkfs.ext4 /dev/sda1`
* `добавить сточку в flashmounter скрипт: sudo -S chown pi $AM`

## Накати-ка образ OrangePiZero на эсдишечку ##

* заходим в рут: `sudo -s`
* должна появиться решётка в консоли
* узнаём как называется устройство, которое суть эсдишка; пусть, к примеру, _/dev/mmcblk0p1_
* размонтируем её подмонтированный раздел, например: `umount /dev/mmcblk0p1`
* кстать, подмонтированных разделов может быть несколько, желательно размонтировать все!
* раздэдэшиваем образ на устройство, например: `dd if=Armbian_5.30_Orangepizero_Ubuntu_xenial_default_3.4.113.img | pv | dd bs=4M of=/dev/mmcblk0`
* пляшем!

## Создание образа SD Card ##

* `sudo su`
* `dd if=/dev/mmcblk0 | pv | gzip -1 | dd of=kontur.img.gz`

## Хорошая статья по настройке Orange ##

* http://www.instructables.com/id/Orange-Pi-One-Setup-Guide/

## Пример /etc/fstab на OrangePiZero для read-only файловой системы ##

### Старая версия ###

```
#!
UUID=d00b548f-42bb-44e4-aa5d-da053db3e25d / ext4 ro,noatime,nodiratime,commit=600,errors=remount-ro 0 1
tmpfs /tmp tmpfs defaults,nosuid 0 0
/var/swap none swap sw 0 0
```

### Новая версия ###

* `sudo armbian-config`
* System >> Overlayroot >> Enable

## Ошибка с Locale на Ubuntu ##

* `sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8`

## Ошибка с Locale на OrangePi, RaspPi ##

* `sudo nano etc/locale.gen` (раскомментировать строчку en_US.UTF-8)
* `sudo nano /etc/default/locale` (~~SKIP~~)
```
#!
LANGUAGE=en_US.UTF-8
LC_ALL=en_US.UTF-8
LANG=en_US.UTF-8
LC_TYPE=en_US.UTF-8
```
* `sudo locale-gen en_US.UTF-8`
* `sudo dpkg-reconfigure locales`

## Отключение запроса пароля на выполнение операций от суперпользователя (stop asking for password) ##

* `sudo su`
* `visudo`
* найди строку: `%sudo    ALL=(ALL:ALL) ALL`
* замени на: `%sudo    ALL=(ALL:ALL) NOPASSWD:ALL`
* `exit` (выход из под root'а)

## Смена IP-адреса на Yarus K2100 ##

* POS-менеджер (перезагрузить терминал и удерживать Меню)
* Конфигурация
* Настройка связи
* LINK1
* Ethernet
* Указать IP-адрес

## Найти директории с большим размером файлов ##

* sudo du -xh / | grep -P "G\t"

# Cython #

## Установка ##

* sudo pip3 install cython --install-option="--no-cython-compile"

## Кодируем файл run.py ##

* cython --embed run.py
* gcc -Os -I /usr/include/python3.5m -o run run.c -lpython3.5m -lpthread -lm -lutil -ldl

## Кодируем файл dog.py ##

* cython --embed dog.py
* gcc -Os -I /usr/include/python3.5m -o dog dog.c -lpython3.5m -lpthread -lm -lutil -ldl

# Прошивка #

## прошивание slave платы с помощью avrdude на Linux с помощью avrisp mkII ##
* sudo apt-get install avrdude
* avrdude -p x32a4u -c avrisp mkII -P usb -v -U flash:w:file.hex:i
если ошибка avrdude: usbdev_open(): did not find any USB device "usb" (0x03eb:0x2104)
то нужно перепрошить avrisp mkII, и повторить команду avrdude -p x32a4u -c avrisp mkII -P usb -v -U flash:w:file.hex:i

## перепрошить avrisp mkII ##
* скачать Amtel Flip
* скачать http://articles.greenchip.com.ua/files/3/43/AllAvrisp%20mkII.zip
* запустить avrisp mkII в режиме прошивки: Подключаем к компютеру, зажимаем HWB, зажимаем RST, отпускаем RST, отпускаем HWB
* запускаем Flip
* Device > Select > AT90USB162
* Иконка USB > USB (или просто Ctrl+U нажимаем)
* File > Load hex file > (Указываем на hex файл в скаченом архиве AllAvrisp mkII.zip)
* Нажимаем кнопку Run
* Переподсоединяем avrisp mkII
